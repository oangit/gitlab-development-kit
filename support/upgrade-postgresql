#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/shellout.rb'
require_relative '../lib/gdk'
require_relative '../lib/gdk/postgresql'

class PostgresUpgrader
  POSTGRESQL_VERSIONS = [11, 10, 9.6].freeze
  TARGET_VERSION = 11

  def initialize(target_version = TARGET_VERSION)
    @target_version = target_version

    check!
  end

  def check!
    puts "Available PostgreSQL versions: #{available_versions}"

    raise "Unable to find target PostgreSQL version #{@target_version}" unless available_versions.include?(@target_version)
    raise "Unable to find current PostgreSQL version #{current_version}" unless available_versions.include?(current_version)
  end

  def needs_upgrade?
    current_version < @target_version
  end

  def upgrade!
    unless needs_upgrade?
      puts "PostgreSQL data directory does not need upgrading"
      return
    end

    gdk_stop
    init_db(target_path)
    pg_upgrade(current_data_dir, target_path)
    promote_new_db(current_data_dir, target_path)
  end

  private

  def target_path
    @target_path ||= File.join(current_data_dir + ".#{@target_version}.#{Time.now.to_i}")
  end

  def gdk_stop
    run!('gdk stop')
  end

  def init_db(target_path)
    cmd = "#{initdb_bin(@target_version)} --locale=C -E utf-8 #{target_path}"
    puts "Initializing database for PostgreSQL #{@target_version}..."
    run!(cmd)
  end

  def pg_upgrade(old_path, new_path)
    cmd = "#{pg_upgrade_bin(@target_version)} -b #{bin_path(current_version)} -B #{bin_path(@target_version)} -d #{old_path} -D #{new_path}"
    puts 'Upgrading PostgreSQL data...'
    run!(cmd)
  end

  def promote_new_db(old_path, new_path)
    backup_path = "#{old_path}.#{current_version}.#{Time.now.to_i}"
    puts "Backing up #{old_path} -> #{backup_path}"
    FileUtils.mv(current_data_dir, backup_path)

    puts "Promoting newly-creating database: #{new_path} -> #{old_path}"
    FileUtils.mv(new_path, old_path)
  end

  def initdb_bin(version)
    File.join(bin_path(version), 'initdb')
  end

  def pg_upgrade_bin(version)
    File.join(bin_path(version), 'pg_upgrade')
  end

  def bin_path(version)
    raise "Invalid PostgreSQL version #{version}" unless available_versions.key?(version)

    available_versions[version]
  end

  def available_versions
    @available_versions ||= brew_cellar_available_versions
  end

  def brew_cellar_available_versions
    POSTGRESQL_VERSIONS.each_with_object({}) do |version, paths|
      brew_cellar_pg = Shellout.new(%W[brew --cellar postgresql@#{version}]).try_run

      next if brew_cellar_pg.empty?

      brew_cellar_pg_bin = Dir.glob(File.join(brew_cellar_pg, '/*/bin'))

      paths[version] = brew_cellar_pg_bin.last if brew_cellar_pg_bin.any?
    end
  end

  def current_data_dir
    @current_data_dir ||= begin
      config = GDK::Config.new
      File.join(config.postgresql.dir, 'data')
    end
  end

  def current_version
    @current_version ||= begin
      path = File.join(current_data_dir, 'PG_VERSION')

      raise "PG_VERSION not found in #{path}. Is PostgreSQL initialized?" unless File.exist?(path)

      version = File.read(path).to_f

      # After PostgreSQL 9.6, PG_VERSION uses a single integer (10, 11, 12, etc.)
      version >= 10 ? version.to_i : version
    end
  end

  def run!(cmd)
    puts cmd

    return if system(cmd)

    puts 'Command failed, exiting'
    exit 1
  end
end

upgrader = PostgresUpgrader.new
upgrader.upgrade!
